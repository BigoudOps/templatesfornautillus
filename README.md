[![Join the Discord channel](https://img.shields.io/static/v1.svg?label=%20Rejoignez-moi%20sur%20Discordl&message=%F0%9F%8E%86&color=7289DA&logo=discord&logoColor=white&labelColor=2C2F33)](https://discord.gg/bfB6Ve6)
![visitors](https://visitor-badge.glitch.me/badge?page_id=BigoudOps.readme)

[![Reddit profile](https://img.shields.io/reddit/subreddit-subscribers/apdm?style=social)](https://www.reddit.com/r/apdm) 

# Français:

 Ceci est un ensemble de modèles pour le menu contextuel de nautilus. Le dossier est configuré pour une installation en français. Si vous désirez l’installer dans votre langue veillez à remplacer le nom “Modèles” par “Templates” en anglais.

 # English :
  
  This is a set of templates for the nautilus context menu. The file is configured for an installation in French. If you want to install it in your language, be sure to replace the name “Modèles” by “Templates” in English.

  