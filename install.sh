#!/bin/bash

if [ -f mode.tar.gz ]; then
    echo "Extracting.."
    tar -xvzf mode.tar.gz

    if [ ! -d ~/Modèles ]; then
        mv Modèles ~/Modèles
        echo -e "\nModèles directory created.."
    else
        rsync -a Modèles/ ~/Modèles/
        rm -rf Modèles
    fi

else
    echo -e "\nmode.tar.gz not found."
    exit 0
fi
echo "Context menu options created.."

# restart nautilus and put it into the background
nautilus -q && nautilus &
echo -e "\nInstallation complete!  You should now have context menu options for new files."
